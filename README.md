Role Name
=========

A brief description of the role goes here.

Requirements
------------

EPEL


Role Variables
--------------

Defaults
```yaml
opt: '/opt'
skel: '/etc/skel'
paths:
  oh-my-zsh: '{{ opt }}/.oh-my-zsh'
  site-functions: "{{ '/usr/local/share/zsh/site-functions' if ansible_distribution == 'Ubuntu' else '/usr/share/zsh/site-functions' }}"
  pure: '{{ opt }}/pure'
  plugins: '{{ opt }}/.oh-my-zsh/custom/plugins'
  zsh-syntax-highlighting: '{{ opt }}/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting'
```

Additionnal variables
```yaml
user: "scherrjo2"
group: "Domain Users"
home: "/home/ldap/{{ user }}"
override_zshrc: false
override_shell: false
zsh_path: "/usr/bin/zsh"
```

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
